import { Component } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
//paginas
import { LoginPage, HomePage, ProfilePage, RootPage, TabsPage } from '../pages/index.pages';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = RootPage;
  pages: Array<{title: string, component: any, icon: any}>

  constructor(platform: Platform,
              statusBar: StatusBar,
              public menuCtlr: MenuController,
              public splashScreen: SplashScreen,
              public storage: Storage) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.storage.ready().then(() =>{
        this.storage.get('loginState').then((value)=>{
          console.log( "state: ", value );
          if( value === true ){
            this.rootPage = TabsPage;
          }else{
            this.rootPage = LoginPage;
          }
        });
      });
      this.pages = [
            { title: 'Dashboard', component: TabsPage    , icon: 'home'},
            { title: 'Perfil'   , component: ProfilePage , icon: 'body'},
          ];
    });
  }

  openPage( p:any ){
    this.rootPage = p;
  }

}
