import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
//providers
import { FreedomApi } from "../providers/freedom-api";
import { Identity } from "../providers/identity";
import { HttpModule } from '@angular/http';
//pages
import { HomePage, LoginPage, ProfilePage, RootPage, TabsPage, SingupPage, ValidatePage } from '../pages/index.pages';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RootPage,
    TabsPage,
    SingupPage,
    ValidatePage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      //backButtonText: 'Go Back',
      //iconMode: 'ios',
      //modalEnter: 'modal-slide-in',
      //modalLeave: 'modal-slide-out',
      tabsPlacement: 'top',
      //pageTransition: 'ios-transition'
    }
  )],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RootPage,
    TabsPage,
    SingupPage,
    ValidatePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FreedomApi,
    Identity,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
