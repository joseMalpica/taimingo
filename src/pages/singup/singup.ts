import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormControl,  FormBuilder } from '@angular/forms';
import { ValidatePage } from "../index.pages";
//providers
import { FreedomApi } from "../../providers/freedom-api";

@IonicPage()
@Component({
  selector: 'page-singup',
  templateUrl: 'singup.html',
})
export class SingupPage {

  signupForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private api: FreedomApi) {
    this.signupForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      nickname: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastname1: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastname2: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      password: ['', Validators.compose([Validators.required]) ],
      phone: ['', Validators.compose([Validators.required])],
      confirm_password: [''],
    });
    /*this.signupForm = new FormGroup({
      email: new FormControl('', Validators.required),
      nickname: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      lastname1: new FormControl('', Validators.required),
      lastname2: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      confirm_password: new FormControl('')
    });*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SingupPage');
  }

  doSignup(){
    let data: any = " { Customer: {"+
            "name:"+ this.signupForm.get('name').value+
            ", surnames:"+ this.signupForm.get('lastname1').value+" "+this.signupForm.get('lastname2').value+
            ", email:"+ this.signupForm.get('email').value+
            ", email_confirm:"+ this.signupForm.get('email').value+
            ", password:"+ this.signupForm.get('password').value+
            ", phone:"+ this.signupForm.get('phone').value+
            ", nickname:"+ this.signupForm.get('nickname').value+
            //"birthday:"+ this.signupForm.get('birthday').value+
            //"gender:"+ this.signupForm.get('gender').value+
            //"newsletter:"+ this.signupForm.get('newsletter').value+
            //"timezone:"+ this.signupForm.get('timeZone').value+
            //"language:"+ this.signupForm.get('language').value+
            //"currency:"+ this.signupForm.get('currency').value+
            "} }";
            this.api.post('/api/app/guest/register', data)
              .subscribe((response)=>{
                  console.log( response );
              },(err)=>{
                console.log( err );
            });

    console.log( data );

    //this.navCtrl.push( ValidatePage );
  }

}
