import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {LoadingController} from "ionic-angular/index";
import { Storage } from '@ionic/storage';
//pages
import { TabsPage, SingupPage } from "../index.pages";
//providers
import { FreedomApi } from "../../providers/freedom-api";
import { Identity } from "../../providers/identity";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading:any;
  login:FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public app:App,
              private storageSession:Storage,
              public loadingCtrl:LoadingController,
              private api: FreedomApi,
              private identity: Identity) {
    this.login = new FormGroup({
            email: new FormControl('malpicadavilajose@gmail.com', Validators.required),
            password: new FormControl('123123', Validators.required)
    });

  }

  ionViewDidLoad() {
  }

  private doLogin(){
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.api.post('app/guest/login', { Customer: this.login.value } )
        .subscribe(
          response => {
            console.log( response.ok );
            if (response.ok === true) {
              this.identity.loadData( response.data );
              if (this.identity.isLogged()) {
                this.login.reset();
                this.storageSession.set( "loginState",true );
                this.loading.dismiss();
                this.navCtrl.setRoot( TabsPage );
              } else {
                this.navCtrl.setRoot( LoginPage );
              }
            } else {
              console.log( "bandera" )
              this.loading.dismiss();
              /*let alert = this.alertCtrl.create({
                title: 'Inicio de sesión',
                message: 'Eso no ha funcionado, por favor verifica tus datos',
                buttons: ['Entendido']

              });
              alert.present().catch(() => {
              });*/
            }
            //this.loading.dismiss();
          },
          error => {
            console.log(error)
            this.loading.dismiss();
            /*let alert = this.alertCtrl.create({
              title: 'Inicio de sesión',
              message: 'Verifica tu conexión',
              buttons: ['Entendido']
            });
            alert.present().catch(()=>{
            });*/
          }
        );
  }

  private goToSignup(){
    this.navCtrl.push( SingupPage );
  }

  private goToForgotPassword(){
    console.log( "Go to sing up page" );
  }

  private doFacebookSignup(){
    console.log( "Go to login with Facebook" );
  }


}
