import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';
import { SettingsModel } from './settings.model';
import { SettingsService } from './settings.service';
//providers
import { Identity } from "../../providers/identity";
import { FreedomApi } from "../../providers/freedom-api";
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [SettingsService]
})
export class ProfilePage {

  settingsForm: FormGroup;
  img = this.identity.photo_circle;
  newsletter: number = 0;
  loading:any;
  //for gender
  testRadioOpen:any;
  testRadioResult:any;
  listing:SettingsModel = new SettingsModel();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingCtrl:LoadingController,
              private alertCtrl: AlertController,
              private api: FreedomApi,
              private settingsService: SettingsService,
              private identity: Identity) {
                this.loading = this.loadingCtrl.create();
                this.settingsForm = new FormGroup({
                      name: new FormControl(),
                      surnames: new FormControl(),
                      email: new FormControl(),
                      nickname: new FormControl(),
                      phone: new FormControl(),
                      birthday: new FormControl(),
                      gender: new FormControl(),
                      timezone: new FormControl(),
                      language: new FormControl(),
                      //Default
                      currency: new FormControl(),
                      weather: new FormControl(),
                      newsletter: new FormControl()
                    });
  }

  ionViewDidLoad() {
    this.settingsForm.get("birthday").setValue( new Date(this.identity.birthday).toISOString() );
    this.settingsForm.setValue({
          name:     this.identity.name,
          surnames: this.identity.surnames,
          email:    this.identity.email,
          nickname: this.identity.nickname,
          phone:    this.identity.phone,
          birthday: this.identity.birthday,
          gender:   this.identity.gender,
          timezone: this.identity.timezone,
          language: 'es',
          currency: 'dollar',
          weather: 'fahrenheit',
          newsletter: this.identity.newsletter,
    });
  }

  newsletterAction(){
    if( true == this.settingsForm.get('newsletter').value){
      this.newsletter = 1;
    }else{
      this.newsletter = 0;
    }
    console.log( this.settingsForm.get('newsletter').value , this.newsletter );
  }

  genderAction(){
        let selectedGender = this.settingsForm.get('gender').value;
        let alert = this.alertCtrl.create();
        alert.setTitle( 'Selecciona tu género' );
        if (selectedGender == 10) {
          alert.addInput({
            type: 'radio',
            label: 'Hombre',
            value: '10',
            checked: true
          });
          alert.addInput({
            type: 'radio',
            label: 'Mujer',
            value: '20',
            checked: false
          });
        } else {
          alert.addInput({
            type: 'radio',
            label: 'Hombre',
            value: "10",
            checked: false
          });
          alert.addInput({
            type: 'radio',
            label: 'Mujer',
            value: "20",
            checked: true
          });
        }
        alert.addButton('Cancelar');
        alert.addButton({
          text: 'Guardar',
          handler: data => {
            this.testRadioOpen = false;
            this.testRadioResult = data;
            //console.log(this.testRadioResult);
            this.settingsForm.get('gender').setValue(this.testRadioResult);
          }
        });
        alert.present();
        this.settingsForm.get('gender').markAsDirty();
  }

  timezoneAction(){
    let alert = this.alertCtrl.create();
        alert.setTitle('Zona horaria');
        this.settingsService
          .getData()
          .then(data => {
            this.listing.zones = data.zones;
            for (let i = 0; i < this.listing.zones.length; i++) {
              alert.addInput({
                type: 'radio',
                label: this.listing.zones[i].zone,
                value: this.listing.zones[i].value,
                checked: false
              });
            }
          });
          alert.addInput({
                      type: 'radio',
                      label: '',
                      value: '',
                      checked: false
                  });

        alert.addButton('Cancel');
        alert.addButton({
          text: 'Guardar',
          handler: data => {
            this.testRadioOpen = false;
            this.testRadioResult = data;
            this.settingsForm.get('timezone').setValue(this.testRadioResult);
          }
        });
        alert.present();
        this.settingsForm.get('timezone').markAsDirty();
        console.log( this.settingsForm.get('timezone').value );
  }

  public saveData(){
    let prompt = this.alertCtrl.create({
          title: 'Contraseña',
          message: "Ingresa tu contraseña para confirmar tus cambios",
          inputs: [
            {
              name: 'password',
              placeholder: ' contraseña',
              type: 'password'
            },
          ],
          buttons: [
            {
              text: 'Cancelar',
              handler: data => {
                console.log( "Cancel clicked! " );
              }
            },
            {
              text: 'Guardar',
              handler: data => {
                this.delegateSentToApi( data );
              }
            }
          ]
        });
        prompt.present();
  }

  public delegateSentToApi( data: any ){
    //data.password (Guardar daros)
    this.loading.present();
    this.api.put('app/customer/save', { Customer: {
                name:          this.settingsForm.get('name').value,
                surnames:      this.settingsForm.get('surnames').value,
                email:         this.settingsForm.get('email').value,
                email_confirm: this.settingsForm.get('email').value,
                nickname:      this.settingsForm.get('nickname').value,
                password:      data.password,
                phone:         this.settingsForm.get('phone').value,
                birthday:      this.settingsForm.get('birthday').value,
                gender:        this.settingsForm.get('gender').value,
                newsletter:    this.newsletter,
                timezone:      this.settingsForm.get('timezone').value,
                language:      this.settingsForm.get('language').value,
                currency:      this.settingsForm.get('currency').value,
            }
        }
    ).subscribe((response)=>{
      console.log( response );
      if( response.ok == true ){
        this.identity.loadData(response.data);
      }else{
        this.showAlert( "Ajustes", "Eso no ha funcionado, por favor verifica tu contraseña" );
      }
    },(error)=>{
      this.showAlert( "Ajustes", "Error interno!" );
    });
    this.loading.dismiss();
    //this.settingsForm.status();
    //document.querySelector("#buttonSave")['style'].display = 'none';
    //(Guardar imagen)
  }

  private showAlert( title: string, msg: string ){
    let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: ['OK']
    });
    alert.present();
  }

}
