export class SettingsModel {
  zones: Array<ListingItemModel>;
}

export class ListingItemModel {
  zone: string;
  value: string;
}

