import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//providers
import { Identity } from "../../providers/identity";
//pages
import { ProfilePage } from "../index.pages";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              private identity: Identity,
              private storageSession:Storage,
            ) {


  }

  ionViewDidLoad() {
    /*this.storageSession.forEach( (value, key, index) => {
    	console.log("This is the value", value)
    	console.log("from the key", key)
    	console.log("Index is", index)
      console.log( " " );
    })
    this.storageSession.get('loginState').then((value)=>{
      console.log( "sate: " , value );
    });*/
  }

  logout(){
    this.identity.logout()
  }

}

/*
  storageSession
    1 identiti -> Customer{

    birthday:"1982-04-20 00:00:00"
    email:"finalmau@gmail.com"
    gender:10
    name:"Mau"
    newsletter:false
    nickname: "Maugento"
    phone:""
    photo_circle:"http://52.15.95.36/taimingo/public/media/cache/circle-160/protected/common/assets/images/image_not_found.png"
    photo_icon:"http://52.15.95.36/taimingo/public/media/cache/circle-32/protected/common/assets/images/image_not_found.png"
    photo_thumbnail:"http://52.15.95.36/taimingo/public/media/cache/150x150/protected/common/assets/images/image_not_found.png"
    surnames:"Pazaran"
    timezone:"America/Central"
    utc_offset:0
    _id:"5755bd7ee1382303e11c7a11"
  }
  2 loginState = true
*/
