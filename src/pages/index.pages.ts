export { LoginPage } from "./login/login";
export { HomePage } from "./home/home";
export { ProfilePage } from "./profile/profile";
export { RootPage } from "./root/root";
export { TabsPage } from "./tabs/tabs";
export { SingupPage } from "./singup/singup";
export { ValidatePage } from "./validate/validate";
